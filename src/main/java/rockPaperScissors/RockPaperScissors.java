package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        
        Random randomnNumber = new Random();
        int rpsChoicesRandom = randomnNumber.nextInt(3);

        String rpsChoices;
        if (rpsChoicesRandom == 0) {
            rpsChoices = "Rock";
        }
        else if (rpsChoicesRandom == 1) {
            rpsChoices = "Paper";
        }
        else {
            rpsChoices = "Scissors";
        }
      
        
        while (true) {
            System.out.println("Let's play round " + roundCounter);
           
            String userInput = readInput ("Your choice (Rock/Paper/Scissors)?");
            
            String humanChoice = userInput;
            String computerChoice = rpsChoices;
            
            String choiceString = ("Human chose " + humanChoice + ", computer chose " + computerChoice);
            
        
            if ((humanChoice.equals("rock") && computerChoice.equals("scissors")) 
            ||  (humanChoice.equals("scissors") && computerChoice.equals("paper")) 
            || (humanChoice.equals("paper") && computerChoice.equals("rock"))){
                System.out.println(choiceString +  " Human wins!");
                humanScore ++;
            }
            else if (humanChoice.equals(computerChoice)) {
                System.out.println(choiceString + " It's a tie!");

            }
            else {
                System.out.println(choiceString + " Computer wins!");
                computerScore ++;
            }
            System.out.println("Score: human " + humanScore + ", computer " + computerScore );
            
            String continueAnswer = readInput("Do you wish to continue playing? (y/n)?");
            if (continueAnswer.equals("n")) {
                break;
            }
            else if (continueAnswer.equals("y")){
                roundCounter ++;
            }

            
            
        }

        System.out.println("Bye bye :)");
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
